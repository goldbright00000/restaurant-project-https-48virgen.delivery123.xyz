<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en" <?php echo method_exists("FunctionsV3",'addClass')?FunctionsV3::addClass($this->is_rtl,'rtl'):''?> >
<head>

<!-- IE6-8 support of HTML5 elements --> 
<!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<link rel="shortcut icon" href="<?php echo  Yii::app()->request->baseUrl; ?>/favicon.ico?ver=1.1" />
<?php 
/*add the analytic codes */
Widgets::analyticsCode();
?>
</head>
<body>


<?php
/*start geolocation for paypal currency converer on the fly */
$location = file_get_contents("http://api.ipstack.com/" . $_SERVER['REMOTE_ADDR'] . "?access_key=f98ad7d373c74f1b7c0b900238024870&fields=country_code");
$ipandcon = explode('":"', $location);
$country = str_replace('"}', '', $ipandcon[1]);
$currency = '';
if($country == 'AL' || $country == 'AD' || $country == 'AM' || $country == 'AT' || $country == 'AZ' || $country == 'BY' || $country == 'BE' || $country == 'BA' || $country == 'BG' || $country == 'HR' || $country == 'CY' || $country == 'CZ' || $country == 'DK' || $country == 'EE' || $country == 'FI' || $country == 'FR' || $country == 'GE' || $country == 'DE' || $country == 'GR' || $country == 'HU' || $country == 'IS' || $country == 'IE' || $country == 'IT' || $country == 'KZ' || $country == 'XK' || $country == 'LV' || $country == 'LI' || $country == 'LT' || $country == 'LU' || $country == 'MK' || $country == 'MT' || $country == 'MD' || $country == 'MC' || $country == 'ME' || $country == 'NL' || $country == 'NO' || $country == 'PL' || $country == 'PT' || $country == 'RO' || $country == 'RU' || $country == 'SM' || $country == 'RS' || $country == 'SK' || $country == 'SI' || $country == 'ES' || $country == 'SE' || $country == 'CH' || $country == 'TR' || $country == 'UA' || $country == 'UK' || $country == 'VA')
{
	$currency = 'EUR';
}
else {
	$currency = 'USD';
}
if(isset($_SESSION['currency'])) {
	$_SESSION['currency'] = $currency;
}
else {
	$_SESSION['currency'] = $currency; 
}
/*end geolocation for paypal currency converer on the fly */
?>